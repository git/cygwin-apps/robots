/*
 * robots.h: include file for the robots game
 */

# include <curses.h>
# include <signal.h>
# include <pwd.h>
# include <ctype.h>
# include <sys/types.h>
# include <errno.h>
# include <time.h>
# include <stdlib.h>
# include <string.h>
# include <unistd.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <stdarg.h>
# ifdef	BSD42
# include <sys/file.h>
# endif /* BSD42 */

# define MIN_ROBOTS	10	/* no. of robots you start with */
# define MAX_ROBOTS	500	/* maximum robots on a screen	*/
# define MIN_VALUE	10	/* what each robot is worth to begin */
# define MAX_FREE	3	/* max free teleports per level	*/
# define FASTEST	2	/* the fastest robot (dont fiddle) */

# define VERT	'|'		/* vertical wall	*/
# define HORIZ	'-'		/* horizontal wall	*/
# define ROBOT	'='		/* normal robot		*/
# define FROBOT '#'		/* fast robot		*/
# define SCRAP  '@'
# define ME	'I'
# define MUNCH	'*'
# define DOT	'.'

# define LEVEL		(level+1)

# define MSGPOS		35	/* where messages appear on bottom line */
# define RVPOS		47

/* These you may want to fiddle with. Position of the two high score files */
#ifdef SCOREDIR
# define HOF_FILE	SCOREDIR "/robots.hof"
# define TMP_FILE	SCOREDIR "/robots.tmp"
#else
# define HOF_FILE	"/usr/local/games/lib/robots.hof"
# define TMP_FILE	"/usr/local/games/lib/robots.tmp"
#endif

# define NUMSCORES	20		/* number of people to record */
# define NUMNAME	"Twenty"	/* above spelt out */

# define TEMP_DAYS	7		/* how long temp scores last */
# define TEMP_NAME	"Week"

# define ROBOTOPTS	"ROBOTOPTS"	/* environment tailoring */

# define MAXSTR		100

# define MULT		1.5		/* multiplier for fast robots */

/* if ALLSCORES Undefined - record top n players */
/* # define ALLSCORES			   record top n scores */

# define SECSPERDAY	86400

# define ctrl(x)	((x)&037)
# define BEL	ctrl('G')

# define	abs(X)  ((X) < 0 ? -(X) : (X))
# define	sign(X) ((X) < 0 ? -1 : (X) > 0)

extern	char	whoami[];
extern	char	my_user_name[];
extern	char	cmd_ch;

extern	bool	moveable_heaps;
extern	bool	sonic_screwdriver;
extern	bool	fastrobots;
extern	bool	show_goodmoves;
extern	bool	show_highscore;
extern	bool	last_stand;
extern	bool	bad_move;
extern	bool	running;
extern	bool	waiting;
extern	bool	first_move;
extern	bool	adjacent;
extern	bool	dead;

extern	int	my_x, my_y;
extern	int	new_x, new_y;
extern	int	count;
extern	int	free_teleports;
extern	int 	dots;
extern	int	robot_value;
extern	int	level;
extern	int	max_robots;
extern	int	scrap_heaps;
extern	int	nrobots_alive;
extern	int	free_per_level;
extern	int	old_free;

extern	long	score;

/* good.c */
extern int blocked(register int, register int, register int, register int);
extern void good_moves ();
extern int isgood(register int, register int);
extern int scan(int, int, int, int);
/* main.c */
extern void draw_screen();
extern void erase_dots();
extern int lk_close(int, char *);
extern int lk_open(char *, int);
extern void munch();
extern void msg(char *, ...);
extern void put_dots();
extern int quit(bool);
extern int readchar();
extern int rnd(int);
extern int rndx();
extern int rndy();
extern int xinc(char);
extern int yinc(char);
/* opt.c */
extern void get_robot_opts(char *);
/* robot.c */
extern void put_robots();
extern void robots(int);
extern void screwdriver();
/* score.c */
extern void scorer();
extern void scoring(bool);
/* user.c */
extern void command();
