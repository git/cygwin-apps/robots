srcdir	= .
VPATH	= $(srcdir)
# just -lcurses for SYS V
#LIBS	= -lcurses -ltermcap
LIBS	= -lncurses
# add -DBSD42 for performance wins on 4.2 or better
#IFDEF	= -DBSD42
# Set compile time options:
# -DALLSCORES          don't take a user only once per highscore
OPTIONS	= -DALLSCORES
CPPFLAGS= -I/usr/include/ncurses
CFLAGS  += $(IFDEF) $(OPTIONS) -DSCOREDIR='"$(scoredir)"'
# Suffix of executables. ".exe" on Windows based systems
EXEEXT	= .exe

LINT	= lint
LLFLAGS = -haxbc
SHAR	= shar
SHFLGS  = 
HEADERS = robots.h
SRC	= good.c main.c opt.c robot.c score.c user.c
OBJS	= good.o main.o opt.o robot.o score.o user.o
MAN	= robots.6
MISC	= READ_ME Makefile
DIST	= $(MISC) $(SRC) $(MAN) $(HEADERS)

bindir  = /usr/bin
mandir  = /usr/share/man/man6
scoredir= /usr/share/robots
postinstalldir=/etc/postinstall
DESTDIR	=

all: robots$(EXEEXT) robots.6

robots$(EXEEXT): ${OBJS}
	$(CC) $(CFLAGS) -o $@ ${OBJS} $(LIBS)

robots.6: robots.0
	m4 $(OPTIONS) -Dscoredir='$(scoredir)' $< > $@

install: robots$(EXEEXT) robots.6
	-mkdir -p $(DESTDIR)$(bindir)
	install robots$(EXEEXT) $(DESTDIR)$(bindir)
	-mkdir -p $(DESTDIR)$(mandir)
	install -c robots.6 $(DESTDIR)$(mandir)
	-mkdir -p $(DESTDIR)$(postinstalldir)
	sed -e 's#@SCOREDIR@#$(scoredir)#' < $(srcdir)/robots.sh > $(DESTDIR)$(postinstalldir)/robots.sh

tidy: clean
clean:
	rm -f robots$(EXEEXT) ${OBJS} core a.out lint.out shar.out *.stackdump robots.6

clobber: clean
	rm -f robots$(EXEEXT) robots.6

lint: ${SRC} $(HEADERS)
	$(LINT) $(LLFLAGS) $(IFDEF) ${SRC} $(LIBS)

shar: shar.out
shar.out: $(DIST)
	$(SHAR) $(SHFLGS) $(DIST) > $@

${OBJS}: ${HEADERS}
